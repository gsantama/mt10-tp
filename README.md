# MT10-TP

TPs de MT10. Uniquement le code Python/SageMath. 
Printemps 2024.

## Installer `SageMath`

Pour installer `SageMath` et lancer les scripts, il suffit d'utiliser le package manager de votre distribution et d'installer le package correspondant (voir [cette page](https://repology.org/project/sagemath/versions)).

## Codes et sujets

- TP1:
  - [Code](https://gitlab.utc.fr/gsantama/mt10-tp/-/tree/main/TP1)
  - [Sujet](https://sites.google.com/view/mt10-sage/exp%C3%A9riences-avec-sagemath/th%C3%A8me-1-petits-groupes)
- TP2:
  - [Code](https://gitlab.utc.fr/gsantama/mt10-tp/-/tree/main/TP2)
  - [Sujet](https://sites.google.com/view/mt10-sage/exp%C3%A9riences-avec-sagemath/th%C3%A8me-2-rsa)
- TP3:
  - [Code](https://gitlab.utc.fr/gsantama/mt10-tp/-/tree/main/TP3)
  - [Sujet](https://sites.google.com/view/mt10-sage/exp%C3%A9riences-avec-sagemath/th%C3%A8me-3-corps-finis-et-codes-correcteurs)
