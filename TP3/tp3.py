from sage.all import *
from copy import deepcopy

N = 6
Q = 2**N
Fq = PolynomialRing(GF(Q, name="a"), "x")


def factorize():
    X = Fq.gen()

    return (X**Q - X).factor()


def is_in_set(r, set=[-1, 0, 1]):
    for i in r:
        if not moebius(i) in set:
            return False
    return True


def euler_moebius(r):
    l = [sum(moebius(k) for k in divisors(n)) for n in r]
    if l[0] != 1:
        return False
    return all(k == 0 for k in l[1:])


def g(f, wj):
    return sum([f(d) for d in divisors(wj)])


def invert_moebius(f, n):
    if n == 1:
        return 1

    a = [(d, moebius(d) * f(n // d)) for d in divisors(n)]
    print(a)
    return sum(moebius(d) * f(n // d) for d in divisors(n))


def Irr(p, n):
    inv = 1 / n
    return inv * sum(moebius(n // d) * p**d for d in divisors(n))


l = dict()

for n in range(1, 11):
    l[n] = dict()
    for p in [2, 3, 5]:
        l[n][p] = Irr(p, n)


def print_csv(dict, filename):
    with open(filename + ".csv", "w") as f:
        for n in dict:
            for p in dict[n]:
                f.write(f"{n},{p},{dict[n][p]}\n")


def get_irreducible(n=2, d=10):
    F = GF(n)["X"]
    polynomials = []
    for deg in range(1, d + 1):
        for p in F.polynomials(deg):
            if p.is_irreducible():
                polynomials.append(p)


q = 2**8
F = GF(q, "a")
R_X = PolynomialRing(F, "X")
X = R_X.gen()


def codeGRS(x, v, a):
    """
    Fonction qui retourne le code GRS
    """
    func = R_X.zero()

    func += sum([list(F)[ord(coef)] * X**i for i, coef in enumerate(x)])

    return [func(a[i]) * vi for i, vi in enumerate(v)]


def lagrange_polynomial(i, a):
    """
    Retourne le polynôme de Lagrange L_i(x) pour un point d'interpolation donné sur
    le corps fini F.
    """
    li = R_X.one()

    for k, al in enumerate(a):
        if k != i:
            li *= (X - al) / (a[i] - al)

    return li


def decodeGRS(y, a, v):
    """
    Décode un code GRS
    """
    n = len(y)

    it = [vf / v[i] for i, vf in enumerate(y)]

    f = R_X.zero()
    for i, fa in enumerate(it):
        li = lagrange_polynomial(i, a)
        f += fa * (li(a[i]) ** (-1)) * li

    x = [f // X**i % X for i in range(f.degree() + 1)]

    return map(lambda i: chr(list(F).index(i)), x)


def errTrans(y, n):
    err = deepcopy(y)
    for _ in range(n):
        err[randint(0, len(err) - 1)] = F(randint(0, next_prime(q)))
    return err


q = 2**8
w = "Message"
k = len(w)
alphas = [randint(0, 2**8 - 1) for _ in range(k)]
v = [randint(0, 2**8 - 1) for _ in range(k)]


def Syndrome(err, a, v, r):
    syndrome = F.zero()

    for i, yi in enumerate(err):
        r = F.zero()

        for j in range(r):
            r += (F(a[i]) * X) ** j

        li = lagrange_polynomial(i, a)
        syndrome += yi * ((F(v[i]) ** (-1)) * (li(a[i]) ** (-1))) * r

    return syndrome.numerator()


def Clef(syndrome, r):
    if syndrome == 0:
        return (-1, -1)

    ul = [1, 0]
    vl = [0, 1]
    rl = [F(1) * X**r, syndrome]
    ql = [0]
    j = 1

    while rl[j].degree() >= r / 2:
        ul.append(ul[j - 1] - ul[j] * ql[j])
        vl.append(vl[j - 1] - vl[j] * ql[j])
        ql.append(rl[j - 1] // rl[j])
        rl.append(rl[j - 1] % rl[j])
        j += 1

    s = vl[j]
    o = rl[j]
    eval = ((s(0)) ** (-1)) * o

    loc = ((s(0)) ** (-1)) * s
    return (loc, eval)


def Erreur(loc, eval, a, v):
    pb = []
    n = len(a)

    for i, alpha in enumerate(a):
        if loc(alpha ** (-1)) == 0:
            pb.append(i)

    ds = [0] * n
    errs = [0] * n
    for i in pb:
        sm = 1
        ab = a[i]
        for j in pb:
            if j == i:
                continue
            else:
                sm *= 1 - a[j] * ab ** (-1)
        ds[i] = -ab * sm

    for i in pb:
        li = lagrange_polynomial(i, a)
        ub = (v[i] ** (-1)) * (li(a[i]) ** (-1))
        errs[i] = (-1) * a[i] * eval(a[i] ** (-1)) * (ub * ds[i]) ** (-1)

    return errs
