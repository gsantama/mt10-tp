from sage.all import *
from sage.crypto.util import bin_to_ascii
import time
import random
import signal


def is_f_prime(n):
    return is_prime(2 ** (2**n) + 1)


def get_first_f_non_prime():
    for k in range(10):
        if not is_f_prime(k):
            return k
    return None


def get_primes(n):
    return [p for p in range(n) if is_prime(p)]


def is_mersenne_prime(n):
    return is_prime(2**n - 1)


def get_factors(n) -> Factorization:
    return factor(n)


def mersenne(p):
    return 2**p - 1


def is_perfect(n: int) -> bool:
    return (sum(divisors(n)) - n) == n


def get_perfects(l: list[tuple[int, int]]) -> list[int]:
    return [p for (n, p) in l if is_perfect(n)]


def lucas_test(s: int) -> bool:
    init = 4

    for _ in range(2, s):
        init = init**2 - 2

    return init == 0


class TimeoutException(Exception):
    pass


# Get the biggest Mersenne Prime we can test with the Lucas test in under 5 minutes
def get_biggest_mersenne_lucas():
    def timeout_handler(signum, frame):  # Custom signal handler
        print("Timed out computation.")
        raise TimeoutException

    init_size = 15000

    signal.signal(signal.SIGALRM, timeout_handler)

    def computation(size):
        try:
            p = random_prime(init_size * 10, lbound=size - 1)
            return (lucas_test(p), True)
        except TimeoutException:
            return (False, False)

    def dichotomic_search(start, end, res=False, nruns=0):
        if start == end:
            return start, res
        mid = (start + end) // 2
        print(f"Run {nruns} - Testing {mid}...")
        signal.alarm(300)
        res, success = computation(mid)
        nruns += 1
        if success:
            return dichotomic_search(mid + 1, end, res, nruns)
        return dichotomic_search(start, mid, res, nruns)

    return dichotomic_search(0, init_size)


def power_r(x, n, y):
    if n == 0:
        return y
    if n % 2 == 0:
        return power_r(x * x, n // 2, y * x)
    return power_r(x * x, (n - 1) // 2, y * x)


def power_t(x, n, y):
    while n > 0:
        if n % 2 == 0:
            x = x * x
            n = n // 2
        else:
            y = y * x
            n = n - 1
    return y


# Modulary exponentiation algorithm, adapted from power_r
def power_mod_r(x, n, N):
    # x = int(str(x), base=2)
    x = mod(x, N)
    if n > 0:
        if n % 2 == 0:
            return mod(power_mod(x**2, n / 2, N), N)
        else:
            return mod(x * power_mod(x**2, (n - 1) / 2, N), N)
    else:
        return x**0


def numerise(string: str, N: int) -> list[int]:
    S3 = BinaryStrings()

    binary = S3.encoding(string)
    lpacks = int((N - 1)).bit_length()

    packets = []

    for i in range(0, len(binary), lpacks):
        packets.append((binary[i : i + lpacks]))

    llength = len(packets[-1])
    if llength < lpacks:
        adj = S3.encoding("\0")[0 : lpacks - llength]
        packets[-1] *= adj  # Extend the last packet with leading zeros

    return [int(str(x), 2) for x in packets]


def alphabetise(packs, N: int) -> str:
    S3 = BinaryStrings()
    lpacks = int((N - 1)).bit_length()

    last = packs[-1]
    packs = [S3((bin(x)[2:]).zfill(lpacks)) for x in packs[:-1]]
    binary = S3("1")

    for _, v in enumerate(packs):
        binary *= v

    last = S3((bin(last)[2:]).zfill(lpacks))
    binary *= last

    binary = str(binary).zfill(8 * ((len(binary) - 1) // 8 + 1))

    return bin_to_ascii(binary)


def factor_limits(int_sizes):
    factor_times = []
    for size in int_sizes:
        base = sum([random.randint(0, 9) * 10**i for i in range(size)])
        integer = base**size
        start = time.time()
        factors = factor(integer)
        print(factors)
        end = time.time()
        timing = end - start
        factor_times.append(timing)
    return factor_times


def get_factor_limits():
    int_sizes = [10, 30, 50, 70, 90, 110]
    factor_times = factor_limits(int_sizes)
    with open("factor_times.csv", "w") as f:
        f.write("size,time\n")
        for i in range(len(int_sizes)):
            f.write(f"{int_sizes[i]},{factor_times[i]}\n")


def cleRSA(min, N=None):
    p = next_prime(random_prime(min))
    q = next_prime(p)

    while p * q == N:
        p = next_prime(random_prime(min))
        q = next_prime(p)

    n = p * q
    phi_n = (p - 1) * (q - 1)

    e = 2
    while gcd(e, phi_n) != 1:
        e += 1

    d = inverse_mod(e, phi_n)

    return (n, e, d)


def get_factortime_average():
    times = []

    for i in range(10):
        print(i)
        n, _, _ = cleRSA(30)
        start = time.time()
        ft = factor(n)
        end = time.time()
        times.append(end - start)

    print(times)
    return sum(times) / len(times)


def encrypt(message: list[int], N, e):
    return list(map(lambda x: power_mod(x, e, N), message))


def decrypt(cipher: list[int], N, d):
    return list(map(lambda x: power_mod(x, d, N), cipher))


# akey = cleRSA(10)  # Alice's Key
# bkey = cleRSA(10, akey[0])  # Bob's Key
# Nc = (min(akey[0], bkey[0]) // 3) - 1  # Nc < 3Na, 3Nb
Nc = 4


def protocole1_enc(msg, sig, N, e, N2, d2):
    m = numerise(msg, Nc)
    s = numerise(sig, Nc)

    print(m)

    c = encrypt(m, N, e)
    s2 = encrypt(s, N2, d2)

    return c, s2


def protocole1_dec(c, s, N, d, N2, e2):
    m = decrypt(c, N, d)
    s = decrypt(s, N2, e2)

    m = alphabetise(m, Nc)
    s = alphabetise(s, Nc)

    return m, s


def protocole2_enc(msg, Na, Da, Nb, Eb, Nc):
    msgc = numerise(msg, Nc)
    pone = encrypt(msgc, Eb if Na > Nb else Da, Nb if Na > Nb else Na)
    return encrypt(pone, Da if Na > Nb else Eb, Na if Na > Nb else Nb)


def protocole2_dec(msgc, Na, Ea, Nb, Db, Nc):
    pone = decrypt(msgc, Ea if Na > Nb else Db, Na if Na > Nb else Nb)
    ptwo = decrypt(pone, Db if Na > Nb else Ea, Nb if Na > Nb else Na)
    return alphabetise(ptwo, Nc)


def fermat_factorization(N):
    A = ceil(sqrt(N))
    Bsq = A * A - N
    while not Bsq.is_square():
        A += 1
        Bsq = A * A - N
    return A - sqrt(Bsq)


# Get all the factors using te fermat factorization
def get_factors_fermat(N):
    factors = []
    while not is_prime(N):
        f = fermat_factorization(N)
        factors.append(f)
        N = N // f
    factors.append(N)
    return factors


def is_perilous(p, q):
    N = (p * q) // 1000
    for L in range(1, N):  # 1
        for R in range(1, N):
            for S in range(1, N):
                if L * N == R * R - S * S:
                    return True

    for k in range(1, p):  # 2
        for l in range(1, q):
            z = p * l - q * k
            if z > 0 and gcd(z, p) == 1 and gcd(z, q) == 1:
                return True

    k, l, m = xgcd(p, q)  # 3
    if abs(m) < q and abs(m) < p:
        return True

    for k in range(1, p):  # 4
        for l in range(1, q):
            if abs(p / q - k / l) < 1 / 1000:
                return True

    return False


def get_non_perilous_RSA():
    p = next_prime(random_prime(2000))
    q = next_prime(p)
    while is_perilous(p, q):
        p = next_prime(random_prime(2000))
        q = next_prime(random_prime(2000))
    return (p, q)


# Miller-Rabbin test
def miller_rabbin(n, k=5):
    if n < 2 or n % 2 == 0:
        return n == 2

    tadic = ZZ.valuation(2)

    r = tadic(n - 1)  # 2-adic valuation of n - 1
    d = (n - 1) // 2**r

    for _ in range(k):
        a = ZZ.random_element(2, n - 1)
        x = power_mod(a, d, n)

        if x == 1 or x == n - 1:
            continue

        for _ in range(r - 1):
            x = power_mod(x, 2, n)
            if x == n - 1:
                break
        else:
            return False

    return True


# Fermat probability test
def fermat_probability(n, k=5):
    if n < 2:
        return False
    for _ in range(k):
        a = ZZ.random_element(2, n - 1)
        if gcd(a, n) != 1:
            return False
        if pow(a, n - 1, n) != 1:
            return False
    return True


# We want to get the smallest composite integer that is detected as prime by the Miller-Rabbin test
# for each k in range(2, 5)


def get_composite_miller_rabbin():
    composites = []
    for k in range(2, 6):
        n = 4
        print(n)
        while True:
            if miller_rabbin(n, k) and not is_prime(n):
                composites.append((n, k))
                break
            n += 1
    return composites


def get_composite_fermat_rabbin():
    composites = []
    for k in range(2, 6):
        n = 4
        print(n)
        while True:
            if fermat_probability(n, k) and not is_prime(n):
                composites.append((n, k))
                break
            n += 1
    return composites
