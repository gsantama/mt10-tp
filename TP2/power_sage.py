from sage.all import *
from random import randint

def power_md(x, n, N):
    return mod(power(x, n), N) 

print(power_mod(randint(1, 100), randint(50, 100), randint(3, 1000)))
