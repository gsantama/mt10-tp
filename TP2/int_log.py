import matplotlib.pyplot as plt
import tikzplotlib
from sage.all import *
import numpy as np
from scipy.integrate import quad


def log_integral(x):
    def integrand(t):
        return 1 / np.log(t)

    result, _ = quad(integrand, 2, x)
    return result


x_values = range(0, 10**4)
primes_y = [prime_pi(x) for x in x_values]
hadamar_y = [(x / ln(x)) if ln(x) != 0 else 0 for x in x_values]
li_y = [log_integral(x) for x in x_values]

plt.plot(x_values, primes_y)
plt.plot(x_values, hadamar_y)
plt.plot(x_values, li_y)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Comparaison de $\\pi(x)$, $\\frac{x}{\\ln(x)}$ et $Li(x)$")
tikzplotlib.save("li.tex")
