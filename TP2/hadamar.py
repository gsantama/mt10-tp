import matplotlib.pyplot as plt
import tikzplotlib
from sage.all import *

x_values = range(0, 2**15)
primes_y = [prime_pi(x) for x in x_values]
hadamar_y = [(x / ln(x)) if ln(x) != 0 else 0 for x in x_values]

plt.plot(x_values, primes_y)
plt.plot(x_values, hadamar_y)
plt.xlabel('x')
plt.ylabel('y')
plt.title('Nombres premiers et théorème des nombres')
tikzplotlib.save("hadamar.tex")
