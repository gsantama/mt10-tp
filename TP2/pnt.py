import matplotlib.pyplot as plt
import tikzplotlib
from sage.all import *

x_values = range(0, 10**4)
y_ptn = [(prime_pi(x) * ln(x)) / x if x != 0 else 0 for x in x_values]

plt.plot(x_values, y_ptn)
plt.xlabel("x")
plt.ylabel("y")
plt.title("Théorème des nombres premiers")
tikzplotlib.save("pnt.tex")
