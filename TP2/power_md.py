from sage.all import * # importing sage to be sure that calculating time average isn't biased by the time it takes to import sage
from random import randint

def power_mod(x, n, N):
    if N == 1:
        return 0
    if n == 0:
        return 1
    if n % 2 == 0:
        r = power_mod(x, n // 2, N)
        return (r * r) % N
    else:
        r = power_mod(x, n // 2, N)
        return (x * r * r) % N


print(power_mod(randint(1, 100), randint(50, 100), randint(3, 1000)))
