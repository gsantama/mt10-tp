from sage.all import *

C4 = groups.permutation.Cyclic(4)
C4_cayley = C4.cayley_table()

C2 = groups.permutation.Cyclic(2)
C2C2 = cartesian_product([C2,C2])

C2C2_cayley = C2C2.cayley_table()
G = groups.permutation.Symmetric(3)

def has_neutral(group):
    cayley = group.cayley_table().table()
    for i in range(len(cayley)):
        if all([cayley[i][j] == j and cayley[j][i] == j for j in range(len(cayley))]):
            return group[i]
    return None

def has_neutral_list(group):
    for i in range(len(group)):
        if all([group[i][j] == j and group[j][i] == j for j in range(len(group))]):
            return group[i]
    return None

def has_neutral_sage(group):
    for e in group:
        if e.is_one():
            return e
    return None

def has_symetric(table, elem):
    for i in range(len(table)):
        if table[i][elem] == 0 and table[elem][i] == 0:
            return i
    return None

def has_symetric_sage(group, elem):
    return group.one() / elem

def is_assoc(table):
    for i in range(len(table)):
        for j in range(len(table)):
            for k in range(len(table)):
                if table[table[i][j]][k] != table[i][table[j][k]]:
                    return False
    return True

def get_indice(elem, group):
    for i in range(len(group)):
        if group[i] == elem:
            return i
    return None

def is_morphism(f, A, B):
    A_cayley = A.cayley_table()
    A_list = A.list()
    B_cayley = B.cayley_table()
    for i in range(len(A.list())):
        for j in range(len(A.list())):
            if B_cayley[(f[i], f[j])] != f[get_indice(A_cayley[A_list[i], A_list[j]], A)]:
                return False
    return True

def get_bijections(A):
    permutations = Permutations(A)
    return permutations.list()

def automorphisms(A):
    bijections = get_bijections(A.list())
    auto = []
    for bij in bijections:
        if is_morphism(bij, A, A):
            auto.append(bij)
    return auto

from sage.combinat.matrices.latin import *
LS4 = dlxcpp_find_completions(LatinSquare(4))

def check_first_col(square, size, col):
    for i in range(size):
        if square[i][0] != col[i]:
            return False
    return True

def group_by(matrix, num):
    return [matrix[i:i+num] for i in range(0, len(matrix), num)]

def filter_latin(latin_squares, size, row, col):
    latin_squares = [ls.list() for ls in latin_squares]
    filtered = []

    latin_squares = [group_by(l, size) for l in latin_squares]

    for ls in latin_squares:
        if check_first_col(ls, size, col) and ls[0] == row:
            filtered.append(ls)

    return filtered

def is_group(table):
    neutral = has_neutral_list(table)
    if neutral is None:
        return False
    for i in range(len(table)):
        if has_symetric(table, i) is None:
            return False
    if not is_assoc(table):
        return False
    
    return True

def filter_groups(latin_squares):
    return [ls for ls in latin_squares if is_group(ls)]

def get_number_orders(group):
    for g in group:
        print("{}   {}".format(g.order(), g))

    orders = map(order, group.subgroups())
    from collections import Counter
    order_count = Counter(orders)

    for i, count in order_count.items():
        print("Order {}: {}".format(i, count))

    return order_count

def is_normal(group, subgroup):
    for g in group:
        for h in subgroup:
            if g*h*g**(-1) not in subgroup:
                return False
    return True

def is_normal_sage(group, subgroup):
    return subgroup.is_normal(group)

group = SymmetricGroup(4)
basic = lambda sub: is_normal(G, sub)
sagemath = lambda sub: is_normal(G, sub)
m = map(basic, group.subgroups())
m_sage = map(sagemath, group.subgroups())

def get_orders(gnot, cnot, swap):
    gnot = [(1, 3), (2, 4)]
    cnot = [(3, 4)]
    swap = [(2, 3)]
    return (
        order(PermutationGroup([gnot, cnot])),
        order(PermutationGroup([swap, cnot])),
        order(PermutationGroup([gnot, swap])),
        order(PermutationGroup([gnot, cnot, swap])),
    )

def count_normal(m):
    count = 0
    for i, bis_normal in enumerate(m):
        if bis_normal:
            count += 1
    return count

def generate_graph(filename, group, layout="circular", generators=None):
    graph = group.cayley_graph(generators=generators)

    figure = graph.plot(layout=layout, figsize=[50, 50], vertex_size=200, vertex_labels=False, edge_style='-', edge_thickness=0.3, edge_labels=False, graph_border=False)
    figure.save(filename)

gnot = [(1, 3), (2, 4)]
cnot = [(3, 4)]
swap = [(2, 3)]

generators = [gnot, cnot, swap]
graph = group.cayley_graph(generators=generators)
is_planar = graph.is_planar()
print(is_planar)